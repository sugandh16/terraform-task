module "label" {
  source     = "./modules/label"
  namespace  = var.namespace
  name       = var.name
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = var.attributes
  #tags       = var.tags
}
module "VPC" {
  source              = "./modules/VPC"
  name                = "${module.label.namespace}-${module.label.stage}"
  namespace           = var.namespace
  stage               = var.stage
  delimiter           = var.delimiter
  attributes          = var.attributes  
  tags                = "${module.label.namespace}-${module.label.stage}"
  #-----------------------------------------------#
  main_vpc_cidr       = var.main_vpc_cidr
  availability_zone1  = var.availability_zone1
  availability_zone2  = var.availability_zone2
  subnet1_cidr        = var.subnet1_cidr
  subnet2_cidr        = var.subnet2_cidr
  subnet3_cidr        = var.subnet3_cidr
  subnet4_cidr        = var.subnet4_cidr
}

/*
This is the 'main' Terraform file. It calls all of our modules in order to
bring up the whole infrastructure
*/

module "EC2" {
####NAME-MODULE#####  
  source    = "./modules/EC2"
  namespace                          = var.namespace
  stage                              = var.stage
  name                               = var.name
  delimiter                          = var.delimiter
  attributes                         = var.attributes
  tags                               = var.tags
  ##Bastion Host##
  bastion_ami                           = var.bastion_ami
  bastion_instance_type                 = var.bastion_instance_type
  key_pair                              = var.key_pair
  subnet3_id_output                     = module.VPC.subnet3_id_output
}
