################ VPC #################
resource "aws_vpc" "main" {
  cidr_block       = "${var.main_vpc_cidr}"
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.tags}-vpc"
  }
}


 ################# Subnets #############
resource "aws_subnet" "subnet1" {
  
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.subnet1_cidr}"
  availability_zone = "${var.availability_zone1}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.tags}-private-1a"
  }
}
resource "aws_subnet" "subnet2" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.subnet2_cidr}"
  availability_zone = "${var.availability_zone2}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.tags}-private-1b"
  }
}
resource "aws_subnet" "subnet3" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.subnet3_cidr}"
  availability_zone = "${var.availability_zone1}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.tags}-public-1a"
  }
}
resource "aws_subnet" "subnet4" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.subnet4_cidr}"
  availability_zone = "${var.availability_zone2}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.tags}-public-1b"
  }
}

######## IGW ###############
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name = "${var.tags}-igw"
  }
}

########### NAT ##############
resource "aws_eip" "nat1" {
    tags = {
      Name = "${var.tags}-eip1"
  }
}

resource "aws_nat_gateway" "natgw1" {
  allocation_id = "${aws_eip.nat1.id}"
  subnet_id     = "${aws_subnet.subnet3.id}"

  tags = {
    Name = "${var.tags}-NATgw1"
  }
}


############# Route Tables ##########

resource "aws_route_table" "public-rt" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags = {
    Name = "${var.tags}-public-rt"
  }
}

resource "aws_route_table" "private-rt-1" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.natgw1.id}"
  }


  tags = {
    Name = "${var.tags}-private-rt-1"
  }
}




########## PRIVATE Subnets assiosation with rotute table ######
resource "aws_route_table_association" "private-1" {
  subnet_id      = "${aws_subnet.subnet1.id}"
  route_table_id = "${aws_route_table.private-rt-1.id}"
}
resource "aws_route_table_association" "private-2" {
  subnet_id      = "${aws_subnet.subnet2.id}"
  route_table_id = "${aws_route_table.private-rt-1.id}"
#  route_table_id = "${aws_route_table.private-rt-2.id}"
}

######### PUBLIC Subnet assiosation with rotute table    ######
resource "aws_route_table_association" "public-3" {
  subnet_id      = "${aws_subnet.subnet3.id}"
  route_table_id = "${aws_route_table.public-rt.id}"
}
resource "aws_route_table_association" "public-4" {
  subnet_id      = "${aws_subnet.subnet4.id}"
  route_table_id = "${aws_route_table.public-rt.id}"
}