variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name, e.g. 'eg' or 'cp'"
}

variable "stage" {
  type        = string
  description = "Stage, e.g. 'prod', 'staging', 'dev', or 'test'"
#  default     = "qa"
}

variable "name" {
  type        = string
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = string
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}


variable "availability_zone1" {
  description = "Avaialbility Zones"
}

variable "availability_zone2" {
  description = "Avaialbility Zones"
}

variable "main_vpc_cidr" {
  description = "CIDR of the VPC"
}

variable "subnet1_cidr" {
  description = "CIDR of subnet1"
}
 
variable "subnet2_cidr" {
  description = "CIDR of subnet2"
}

variable "subnet3_cidr" {
  description = "CIDR of subnet3"
}
variable "subnet4_cidr" {
  description = "CIDR of subnet4"
}
