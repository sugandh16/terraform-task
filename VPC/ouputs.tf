

output "vpc_id_output" {
  description = "VPC ID output"
  value = "${aws_vpc.main.id}"
}

output "subnet1_id_output" {
  description = "Subnet1 id output"
  value = "${aws_subnet.subnet1.id}"
}



output "subnet2_id_output" {
  description = "Subnet2 id output"
  value = "${aws_subnet.subnet2.id}"
}
output "subnet3_id_output" {
  description = "Subnet3 id output"
  value = "${aws_subnet.subnet3.id}"
}
output "subnet4_id_output" {
  description = "Subnet4 id output"
  value = "${aws_subnet.subnet4.id}"
}
output "igw_output" {
  description = "IGW id output"
  value = "${aws_internet_gateway.igw.id}"
}



