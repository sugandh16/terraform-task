##EC2
#
resource "aws_security_group" "bastion_sg" {
  name        = "${var.bastion-sg}-bastion-sg"
  description = "Allowinbound traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.bastion-sg}"
  }

}
resource "aws_instance" "bastion" {
  ami           = "${var.bastion_ami}"
  instance_type = "${var.bastion_instance_type}"
  subnet_id     = "${var.subnet3_id_output}"
  key_name      = "${var.key_pair}"
  vpc_security_group_ids = ["${aws_security_group.bastion_sg.id}"]
  tags = {
    Name = "${var.bastion-sg}"
  }
}