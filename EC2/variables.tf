########Bastion-Host#########
#
variable "bastion_ami" {
  description = "AMI to be used inside bastion host"
  #default = "ami-08b3278ea6e379084"
}
variable "bastion_instance_type" {
  description = "Instance type"
  #default = "t2.micro"
}
variable "key_pair" {
  description = "Key pair used to login inside the bastion host"
}
variable "subnet3_id_output" {
  description = "Subnet in which bastion host is deployed"
}